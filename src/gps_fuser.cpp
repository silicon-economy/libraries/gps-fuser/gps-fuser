// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3


// Implementation of the GPSFuser
// Author: Sebastian Hoose (sebastian.hoose@iml.fraunhofer.de)

#include <math.h>
#include <iostream>
#include "gps_fuser.h"

// public

GPSFuser::GPSFuser(double max_time_delay, const sensor_data_classes::Pose& initial_pose) {
  // initialize last_fused_odometry_data
  this->last_fused_odometry_data.pose.orientation.setIdentity();
  this->last_fused_odometry_data.pose.orientation_covariance.setIdentity();
  this->last_fused_odometry_data.pose.position = Eigen::Vector3d::Zero();
  this->last_fused_odometry_data.pose.position_covariance.setIdentity();
  this->last_fused_odometry_data.twist.angular = Eigen::Vector3d::Zero();
  this->last_fused_odometry_data.twist.angular_covariance.setIdentity();
  this->last_fused_odometry_data.twist.linear = Eigen::Vector3d::Zero();
  this->last_fused_odometry_data.twist.linear_covariance.setIdentity();
  this->last_fused_odometry_data.time_stamp = 0.0;

  this->initial_pose_in_fixed_frame = initial_pose;
  this->max_time_delay = max_time_delay;
}

GPSFuser::~GPSFuser() = default;

void GPSFuser::updateGPSData(const sensor_data_classes::GPSData& gps_data) {
  // get index of currently published data
  size_t index_of_current_frame_id = std::find_if(all_current_gps_data.begin(),
                                                  all_current_gps_data.end(),
                                                  [&](const sensor_data_classes::GPSData& toFind) {
                                                    return toFind.origin_frame_id == gps_data.origin_frame_id;
                                                  }) - all_current_gps_data.begin();

  // update data
  if (index_of_current_frame_id == all_current_gps_data.size()) {
    all_current_gps_data.push_back(gps_data);
  } else {
    all_current_gps_data[index_of_current_frame_id] = gps_data;
  }
}

void GPSFuser::calculateEquirectualProjection(double latitude, double longitude, double altitude, double radius_of_earth, double &x, double &y, double &z) {
  // using the approach to locally approximate the earth ellipsoid using a sphere
  x = radius_of_earth * std::cos(latitude*(M_PI/180.0)) * std::cos(longitude*(M_PI/180.0));
  y = radius_of_earth * std::cos(latitude*(M_PI/180.0)) * std::sin(longitude*(M_PI/180.0));
  z = altitude;
}

void GPSFuser::calculateWGS84Projection(double latitude, double longitude, double altitude, double &x, double &y, double &z) {
  const double a = 6378137.0;  // radius of earth [m] (semi-major axis) according to WGS84 definition
  const double e_squared = 0.006694380004260827;  // first numerical eccentricy of earth
  const double chi = std::sqrt(1.0 - (e_squared * std::sin(latitude) * std::sin(latitude)));

  x = (a/chi + altitude) * std::cos(latitude) * std::cos(longitude);
  y = (a/chi + altitude) * std::cos(latitude) * std::sin(longitude);
  z = (((a*(1-e_squared))/chi) + altitude) * std::sin(latitude);
}

bool GPSFuser::getCurrentPose(double current_time_in_secs, sensor_data_classes::OdometryData& pose) {
  // check if there is data which is to old to be used
  if (!checkForOldData(current_time_in_secs)) {
    return false;
  }

  if (!this->all_current_gps_data.empty()) {
    if (!this->initial_position_in_world_has_been_calculated) {
      this->initial_position_in_world = calculateInitialPoseInWorld(all_current_gps_data);
      this->last_fused_odometry_data.pose.orientation = this->initial_position_in_world.orientation;
      this->initial_position_in_world_has_been_calculated = true;
    }

    // calculate odometries per sensor
    std::vector<sensor_data_classes::OdometryData> odom_list;
    calculateOdometryPerSensor(odom_list);

    // cumulate odometry from the odometry which has been calculated for each sensor
    sensor_data_classes::OdometryData fused_odom;
    fused_odom.time_stamp = current_time_in_secs;
    cumulateOdometry(odom_list, fused_odom);

    // calculate twist from current pose and current odometry
    fused_odom.twist = calculateTwists(last_fused_odometry_data, fused_odom);

    // return odom
    pose = fused_odom;

    last_fused_odometry_data = fused_odom;
    return true;
  }

  // return false since there is no data to aggregate
  return false;
}

// private

bool GPSFuser::checkForOldData(double current_time_in_secs) const {
  for (sensor_data_classes::GPSData data : this->all_current_gps_data) {
    if (current_time_in_secs - data.time_stamp > this->max_time_delay) {
      std::cout << "[GPSFuser] One or multiple GPS sensor data is to old according to \"max_time_delay\". Current delay: "
                << current_time_in_secs - data.time_stamp << " seconds" << std::endl;
      return false;
    }
  }
  return true;
}

void GPSFuser::updateSizeOfRotationTrackingVectors(std::vector<std::vector<Eigen::Vector3d>> &rotation_tracking_vectors) const {
  if (rotation_tracking_vectors.size() < this->all_current_gps_data.size()) {
    rotation_tracking_vectors.resize(this->all_current_gps_data.size(),
                                     std::vector<Eigen::Vector3d>(all_current_gps_data.size() - 1,
                                     Eigen::Vector3d::Zero()));
  }
  for (auto it = rotation_tracking_vectors.begin(); it != rotation_tracking_vectors.end(); ++it) {
    if (it->size() < this->all_current_gps_data.size() - 1) {
      it->resize(this->all_current_gps_data.size() - 1);
    }
  }
}

void GPSFuser::calculateOdometryPerSensor(std::vector<sensor_data_classes::OdometryData> &odoms_per_sensor) {
  // remember vectors between GPS sensors and enlarge saving datastructure if necessary for rotation calculation
  static std::vector<std::vector<Eigen::Vector3d>> rotation_tracking_vectors(this->all_current_gps_data.size(),
                                                                             std::vector<Eigen::Vector3d>(all_current_gps_data.size(),
                                                                             Eigen::Vector3d::Zero()));
  updateSizeOfRotationTrackingVectors(rotation_tracking_vectors);

  // iterating through all gps inputs
  for (auto it1 = all_current_gps_data.begin(); it1 != all_current_gps_data.end(); ++it1) {
    // TRANSLATION
    sensor_data_classes::OdometryData odom_based_on_current_sensor;

    // calculate position in robot coordinates
    odom_based_on_current_sensor.pose.position = it1->data - this->initial_position_in_world.position - it1->translation_to_base;
    odom_based_on_current_sensor.pose.position_covariance.setIdentity();


    // ROTATION
    if (all_current_gps_data.size() == 1) {
      // in case of a single valid GPS sensor, setting a valid but constant, zero rotation and rotation velocity
      odom_based_on_current_sensor.pose.orientation.setIdentity();
      odom_based_on_current_sensor.twist.angular = Eigen::Vector3d::Zero();
      odom_based_on_current_sensor.pose.orientation_covariance.setIdentity();
    } else {
      std::vector<double> angles;
      // in case of multiple GPS sensors: calculate rotation for all combination of sensors and cumulate all calculated angles
      for (std::vector<sensor_data_classes::GPSData>::iterator it2 = it1; it2 != all_current_gps_data.end() - 1; ++it2) {
        if (it2 - all_current_gps_data.begin() != it1 - all_current_gps_data.begin()) {
          Eigen::Vector3d vector_between_gps_sensors_in_world = it2->data - it1->data;
          Eigen::Vector3d last_vector_between_sensors_in_world = rotation_tracking_vectors[it1 - all_current_gps_data.begin()][it2 - all_current_gps_data.begin()];
          double changed_angle = calculateAngleBetweenPoints(last_vector_between_sensors_in_world, vector_between_gps_sensors_in_world);
          if (!isnan(changed_angle)) {
            angles.push_back(changed_angle);
          }
          std::cout << it1 - all_current_gps_data.begin() << " " << it2 - all_current_gps_data.begin() << std::endl;
          rotation_tracking_vectors[it1 - all_current_gps_data.begin()][it2 - all_current_gps_data.begin()] = vector_between_gps_sensors_in_world;
        }
      }

      // use mean rotation between all sensors as the filterd rotation
      double cumulated_angle = 0.0;
      if (!angles.empty()) {
        for (double angle : angles) {
          cumulated_angle += angle;
        }
        cumulated_angle /= static_cast<double>(angles.size());
      }
      Eigen::Vector3d yaw_pitch_roll = last_fused_odometry_data.pose.orientation.toRotationMatrix().eulerAngles(2, 1, 0);
      double yaw = yaw_pitch_roll[0] + cumulated_angle < 2.0 * M_PI ?
                   yaw_pitch_roll[0] + cumulated_angle : yaw_pitch_roll[0] + cumulated_angle - 2.0 * M_PI;
      odom_based_on_current_sensor.pose.orientation = Eigen::AngleAxisd(yaw_pitch_roll[2], Eigen::Vector3d::UnitX())
                                                      * Eigen::AngleAxisd(yaw_pitch_roll[1], Eigen::Vector3d::UnitY())
                                                      * Eigen::AngleAxisd(yaw, Eigen::Vector3d::UnitZ());
      odom_based_on_current_sensor.pose.orientation.normalize();
    }
    odoms_per_sensor.push_back(odom_based_on_current_sensor);
  }
}

void GPSFuser::cumulateOdometry(std::vector<sensor_data_classes::OdometryData>& odom_list, sensor_data_classes::OdometryData& cumulated_odom) const {
  // cumulation of all odometries
  const double weight_of_odom = 1.0/static_cast<double>(odom_list.size());

  Eigen::MatrixXd Q(4, odom_list.size());
  for (auto it = odom_list.begin(); it != odom_list.end(); ++it) {
    // calculate the weighted sum for the position
    cumulated_odom.pose.position += weight_of_odom * it->pose.position;

    // creating a matrix of all orientations (which are quaternions)
    Q.col(it - odom_list.begin()).x() = weight_of_odom * it->pose.orientation.x();
    Q.col(it - odom_list.begin()).y() = weight_of_odom * it->pose.orientation.y();
    Q.col(it - odom_list.begin()).z() = weight_of_odom * it->pose.orientation.z();
    Q.col(it - odom_list.begin()).w() = weight_of_odom * it->pose.orientation.w();
  }

  // averaging quaternion using the eigenvector with the largest eigenvalue of QQ^T where Q is a matrix where each column is a weighted Quaternion
  // see: https://stackoverflow.com/questions/12374087/average-of-multiple-quaternions
  // and: https://www.acsu.buffalo.edu/%7Ejohnc/ave_quat07.pdf

  Eigen::MatrixXd Q_T = Q.transpose();
  Eigen::MatrixXd QQ_T = Q*Q_T;

  Eigen::EigenSolver<Eigen::MatrixXd> eigen_handler(QQ_T);
  const Eigen::VectorXcd eigen_values = eigen_handler.eigenvalues();
  Eigen::MatrixXcd eigen_vectors_matrix = eigen_handler.eigenvectors();

  int largest_eigenvalue_index = 0;
  for (int i = 1; i < eigen_values.rows(); ++i) {
    if (eigen_values[largest_eigenvalue_index].real() < eigen_values[i].real()) {  // real values are enough to compare since quaternions are always positive
      largest_eigenvalue_index = i;
    }
  }

  cumulated_odom.pose.orientation.x() = eigen_vectors_matrix.col(largest_eigenvalue_index)(0, 0).real();
  cumulated_odom.pose.orientation.y() = eigen_vectors_matrix.col(largest_eigenvalue_index)(1, 0).real();
  cumulated_odom.pose.orientation.z() = eigen_vectors_matrix.col(largest_eigenvalue_index)(2, 0).real();
  cumulated_odom.pose.orientation.w() = eigen_vectors_matrix.col(largest_eigenvalue_index)(3, 0).real();

  // normalize quaternion to guaranty a valid quaternion
  cumulated_odom.pose.orientation.normalize();

  // calculate covariance of pose
  for (sensor_data_classes::OdometryData odom : odom_list) {
    cumulated_odom.pose.position_covariance(0, 0) += weight_of_odom * std::pow(odom.pose.position.x() - cumulated_odom.pose.position.x(), 2.0);
    cumulated_odom.pose.position_covariance(1, 1) += weight_of_odom * std::pow(odom.pose.position.y() - cumulated_odom.pose.position.y(), 2.0);
    cumulated_odom.pose.position_covariance(2, 2) += weight_of_odom * std::pow(odom.pose.position.z() - cumulated_odom.pose.position.z(), 2.0);

    Eigen::Vector3d odom_yaw_pitch_roll = odom.pose.orientation.toRotationMatrix().eulerAngles(2, 1, 0);
    Eigen::Vector3d cumulated_odom_yaw_pitch_roll = cumulated_odom.pose.orientation.toRotationMatrix().eulerAngles(2, 1, 0);

    cumulated_odom.pose.orientation_covariance(0, 0) = weight_of_odom * std::pow(odom_yaw_pitch_roll.z() - cumulated_odom_yaw_pitch_roll.z(), 2.0);
    cumulated_odom.pose.orientation_covariance(1, 1) = weight_of_odom * std::pow(odom_yaw_pitch_roll.y() - cumulated_odom_yaw_pitch_roll.y(), 2.0);
    cumulated_odom.pose.orientation_covariance(2, 2) = weight_of_odom * std::pow(odom_yaw_pitch_roll.x() - cumulated_odom_yaw_pitch_roll.x(), 2.0);
  }
}

sensor_data_classes::TwistWithCovariance GPSFuser::calculateTwists(const sensor_data_classes::OdometryData& lastPose, const sensor_data_classes::OdometryData& current_odometry) const {
  sensor_data_classes::TwistWithCovariance twist;

  // time since last update
  double dt = current_odometry.time_stamp - lastPose.time_stamp;

  // calculation of linear twist
  twist.linear = (current_odometry.pose.position - lastPose.pose.position) / dt;

  // transformation of angle spaces
  Eigen::Vector3d current_odometry_yaw_pitch_roll = current_odometry.pose.orientation.toRotationMatrix().eulerAngles(2, 1, 0);
  Eigen::Vector3d last_pose_yaw_pitch_roll = lastPose.pose.orientation.toRotationMatrix().eulerAngles(2, 1, 0);

  // calucation of anglular twist
  twist.angular.x() = (last_pose_yaw_pitch_roll.z() - current_odometry_yaw_pitch_roll.z()) / dt;
  twist.angular.y() = (last_pose_yaw_pitch_roll.y() - current_odometry_yaw_pitch_roll.y()) / dt;
  twist.angular.z() = (last_pose_yaw_pitch_roll.x() - current_odometry_yaw_pitch_roll.x()) / dt;

  // calculate covariance of the twist by using the covariance of the pose
  twist.linear_covariance = (1.0/dt) * current_odometry.twist.linear_covariance;
  twist.angular_covariance = (1.0/dt) * current_odometry.twist.angular_covariance;
  return twist;
}

double GPSFuser::calculateAngleBetweenPoints(Eigen::Vector3d v1, Eigen::Vector3d v2) const {
  return std::atan2(v1.x()*v2.y() - v1.y()*v2.x(), v1.x()*v2.x() + v1.y()*v2.y());
}

sensor_data_classes::Pose GPSFuser::calculateInitialPoseInWorld(std::vector<sensor_data_classes::GPSData> sensor_data) const {
  // initialize return value
  sensor_data_classes::Pose fused_data;
  fused_data.position.setZero();

  // calculate mean
  for (sensor_data_classes::GPSData data : sensor_data) {
    fused_data.position += data.data - data.translation_to_base;
  }
  fused_data.position /= static_cast<double>(sensor_data.size());

  fused_data.position -= this->initial_pose_in_fixed_frame.position;

  // using initial_pose_in_fixed_frame as the initial orientation since
  fused_data.orientation = this->initial_pose_in_fixed_frame.orientation;

  return fused_data;
}
