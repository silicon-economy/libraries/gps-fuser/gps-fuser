// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3


// The sensor_data_classes header
// Author: Sebastian Hoose (sebastian.hoose@iml.fraunhofer.de)

#ifndef DATA_CLASSES_H_  // NOLINT(build/header_guard)
#define DATA_CLASSES_H_  // NOLINT(build/header_guard)

#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <string>
#include <vector>

namespace sensor_data_classes {

struct Pose {
  Pose() noexcept {
    orientation.setIdentity();
  }
  Eigen::Vector3d position = Eigen::Vector3d::Zero();
  Eigen::Quaterniond orientation;
};

/**
 * \brief Modeling a pose with covariance
 */
struct PoseWithCovariance {
  PoseWithCovariance() noexcept {
    position_covariance.setIdentity();
    orientation.setIdentity();
    orientation_covariance.setIdentity();
  }

  Eigen::Vector3d position = Eigen::Vector3d::Zero();
  Eigen::Matrix3d position_covariance;
  Eigen::Quaterniond orientation;
  Eigen::Matrix3d orientation_covariance;
};

/**
 * \brief Modelling a twist with covariance
 */
struct TwistWithCovariance {
  TwistWithCovariance() noexcept {
    linear_covariance.setIdentity();
    angular_covariance.setIdentity();
  }
  Eigen::Vector3d linear = Eigen::Vector3d::Zero();
  Eigen::Matrix3d linear_covariance;
  Eigen::Vector3d angular = Eigen::Vector3d::Zero();
  Eigen::Matrix3d angular_covariance;
};


/**
 * \brief Modelling received Odometry data with its covariance
 */
struct OdometryData {
  double time_stamp = 0.0;
  PoseWithCovariance pose;
  TwistWithCovariance twist;
};

/**
 * \brief Modelling received data from a GPS sensor in a cartesian coordinate system with its covariance.
 */
struct GPSData {
  double time_stamp = 0.0;
  std::string origin_frame_id = "";
  Eigen::Vector3d data = Eigen::Vector3d::Zero();
  Eigen::Vector3d translation_to_base = Eigen::Vector3d::Zero();
  std::vector<double> position_covariance;
  enum class CovarianceType {
    COVARIANCE_TYPE_UNKNOWN = 0,
    COVARIANCE_TYPE_APPROXIMATED = 1,
    COVARIANCE_TYPE_DIAGONAL_KNOWN = 2,
    COVARIANCE_TYPE_KNOWN = 3
  };
  CovarianceType position_covariance_type = CovarianceType::COVARIANCE_TYPE_UNKNOWN;
};

}  // namespace sensor_data_classes

#endif  // DATA_CLASSES_H_   // NOLINT(build/header_guard)
