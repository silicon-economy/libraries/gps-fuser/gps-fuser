// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3


// The GPSFuser header
// Author: Sebastian Hoose (sebastian.hoose@iml.fraunhofer.de)

#ifndef GPS_FUSER_H_  // NOLINT(build/header_guard)
#define GPS_FUSER_H_  // NOLINT(build/header_guard)

#include <vector>
#include "./data_classes.h"

class GPSFuser {
 public:
  /**
   * \brief Standard constructor of the GPSFuser class 
   */
  GPSFuser() : max_time_delay(10.0) {}

  /**
   * \brief Constructor of the GPSFuser class 
   * \param max_time_delay maximum delay of sensor data in seconds
   */
  explicit GPSFuser(const double max_time_delay, const sensor_data_classes::Pose& initial_pose_in_fixed_frame);

  /**
   * \brief Destructor of the GPSFuser class
   */
  ~GPSFuser();

  /**
   * \brief Updates the currently received GPS data
   * \param gps_data The new GPS data which should be added to the GPSFusers calculation
   */
  void updateGPSData(const sensor_data_classes::GPSData& gps_data);

  /**
   * \brief Extract the currently fused pose
   * \param pose The current result of the GPSFuser
   * \param current_time_in_secs The current time in seconds [s]
   * \return true if a calculation was possible, false else
   */
  bool getCurrentPose(double current_time_in_secs, sensor_data_classes::OdometryData& pose);  // NOLINT(runtime/references)

  /**
   * \brief Calculation of the equirectual projection for a given GPS position and radius of the earth approximating e.g. the WGS84 reference ellipsoid with a sphere
   * \param latitude GPS latitude in the GPS coordinate frame which should be transformed
   * \param longitude GPS longitude in the GPS coordinate frame which should be transformed
   * \param altitude GPS altitude in the GPS coordinate frame which should be transformed (currently unused)
   * \param radius_of_earth The radius of earth [m] for which the sphere approximation should be calculated (e.g. radius_of_earth at starting position)
   * \param x part of the transformed position in cartesian coordinates
   * \param y part of the transformed position in cartesian coordinates
   * \param z part of the transformed position in cartesian coordinates (currently unused)
   */
  static void calculateEquirectualProjection(double latitude, double longitude, double altitude, double radius_of_earth, double &x, double &y, double &z);  // NOLINT(runtime/references)

  /**
   * \brief Calculation of the WGS84 projection for a given GPS position
   * \param latitude GPS latitude in the GPS coordinate frame which should be transformed
   * \param longitude GPS longitude in the GPS coordinate frame which should be transformed
   * \param altitude GPS altitude in the GPS coordinate frame which should be transformed (currently unused)
   * \param x part of the transformed position in cartesian coordinates
   * \param y part of the transformed position in cartesian coordinates
   * \param z part of the transformed position in cartesian coordinates (currently unused)
   */
  static void calculateWGS84Projection(double latitude, double longitude, double altitude, double &x, double &y, double &z);  // NOLINT(runtime/references)
 private:
  /**
   * \brief Extends the size of the rotation vectors if new GPS sensors have been added during runtime
   * \param rotation_tracking_vectors reference to the   
   */
  void updateSizeOfRotationTrackingVectors(std::vector<std::vector<Eigen::Vector3d>> &rotation_tracking_vectors) const;  // NOLINT(runtime/references)

  /**
   * \brief Cleans out all data from \c all_current_gps_data which is to old
   * \param current_time_in_secs The current time in seconds [s]
   * \returns true if no data was cleaned out, false else
   */
  bool checkForOldData(double current_time_in_secs) const;

  /**
   * \brief Calculates the current odometry per sensor
   * 
   * The current position is calculated by correcting the 
   * measured position using the initial position in the world 
   * and the translation to the tracking frame (e.g. the base of the robot).
   * The rotation per sensor is calculated by either setting it to the identity
   * matrix if only one sensor is available or by calculating the mean rotation
   * of all changed vector orientations. The vectors used for this calculation are
   * represented by all vectors between the current sensor and all other sensors.
   * 
   * \param odoms_per_sensor The odometry which has been calculated per sensor
   */
  void calculateOdometryPerSensor(std::vector<sensor_data_classes::OdometryData> &odoms_per_sensor);  // NOLINT(runtime/references)

  /**
   * \brief Cumulating odometry from a given list of odometry using averaging for every entry
   * \param odomList Vector of odometries that should be cumulated
   * \param cumulatedOdom Cumulated odometry
   */
  void cumulateOdometry(std::vector<sensor_data_classes::OdometryData>& odom_list, sensor_data_classes::OdometryData& cumulated_odom) const;  // NOLINT(runtime/references)

  /**
   * \brief Calculates the twists of the odometry from the last and current pose (given as odometry)
   * \param lastPose Last odometry that has been calculated
   * \param currentPose Currently calculated pose
   * \return Twists of the odometry
   */
  sensor_data_classes::TwistWithCovariance calculateTwists(const sensor_data_classes::OdometryData& lastPose, const sensor_data_classes::OdometryData& current_odometry) const;

  /**
   * \brief Calculates the angle between the two vectors v1 and v2
   * 
   * For this calculation only the x and y part of each vector is used.
   * 
   * \param v1 First of the two vectors
   * \param v2 Secton of the two vectors
   * \returns Angle in rad between two vectors
   */
  double calculateAngleBetweenPoints(Eigen::Vector3d v1, Eigen::Vector3d v2) const;

  /**
   * \brief Calculates the initial position in the world
   * \param sensor_data A set of GPS sensor data which is fused to calculate the initial position
   * \returns Fused initial position
   */
  sensor_data_classes::Pose calculateInitialPoseInWorld(std::vector<sensor_data_classes::GPSData> sensor_data) const;

  sensor_data_classes::OdometryData last_fused_odometry_data;
  std::vector<sensor_data_classes::GPSData> all_current_gps_data;

  sensor_data_classes::Pose initial_position_in_world;
  bool initial_position_in_world_has_been_calculated = false;

  // parameters
  double max_time_delay = 10.0;
  sensor_data_classes::Pose initial_pose_in_fixed_frame;
};

#endif  // GPS_FUSER_H_   // NOLINT(build/header_guard)
