> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

# GPS Fuser Library

This project contains a C++ library which fuses multiple GPS sensor inputs to one position. In addition, a transformation from GPS coordinates to cartesian coordinates takes place.

## Requirements

The only requirement of this library is the [Eigen 3](https://eigen.tuxfamily.org) project which again is a C++ template library for linear algebra. Nonetheless, [Eigen 3](https://eigen.tuxfamily.org) can be installed manually as descirbed on the [Eigen 3](https://eigen.tuxfamily.org) website or by installing [Eigen 3](https://eigen.tuxfamily.org) using a package manager. For Debian based Linux distributions such as Ubuntu `sudo apt install libeigen3-dev` can be used.

This library has been tested using [Eigen 3.3.9](https://gitlab.com/libeigen/eigen/-/tree/3.3.9), an older version is might be sufficient as well.

## Standalone Building

To build this library in standalone mode, the following commands need to be executed:

```
# in root directory of library
mkdir build && cd build
cmake ..
make
```

In addition, the following cmake options can be set:

| Option   | Selections | Description                                                          | Default |
|---       |---         |---                                                                   | ---     |
| BUILD_TESTS  | ON or OFF  | Tests are build if ON is selected                                    | OFF     |
| BUILD_SHARED_LIBS   | ON or OFF  | Library is build static if OFF is selected, shared if ON is selected | OFF      | 

## Installation

Currently, this project can be used as a [cmake](https://cmake.org/) project. To make use of this library using [cmake](https://cmake.org/) two things need to be done:

1. Add this project to your project by e.g. adding the following lines / modifying the following lines in your project.

```cmake
# adding this libraries folder
add_subdirectory(lib/gps_fuser/ EXCLUDE_FROM_ALL)

# adding dependencies
add_dependencies(<target> <your other dependencies> gps_fuser)

# linking this library to your project
target_link_libraries(<target> <your other linked libraries> gps_fuser)
```

2. Include the main header of this project where you want to use this library.

```c++
/*
 * In your .h or .cpp file where you want to use this library
 */
 #include "<path to this library>/include/gps_fuser.h"
```

## Usage

This library has a standard interface, which allow an easy abstracted usage of the GPS Fuser. In detail, only a few functions need to be used after installing the library as presented in the Installation section above.

A detailed description of those functions can be found in [include/position_fuser.h](include/position_fuser.h). A description of the used data classes can be found in [include/data_classes.h](include/data_classes.h).

In detail, only three functions need to be used to fuse one or multiple GPS Data sources to one odometry. The usage of those functions and the used data classes are further discussed below.

#### Data Classes

To interact with the GPSFuser library multiple data classes have been created which can be found in [include/data_classes.h](include/data_classes.h). There is no logic implemented in those data classes. A simple example to use one of those data classes can be found below:

```c++
SensorDataClasses::GPSData gps_data;
gps_data.time_stamp = some_time_stamp;
```

#### Inserting new Data into the GPSFuser

To work properly, the data of the GPSFuser needs to be updated frequently. For this purpose the `void update_gps_data(const SensorDataClasses::GPSData& gps_data);` function has to be used. A simple usage example is given below:

```c++
SensorDataClasses::GPSData gps_data;
gps_data.time_stamp = some_time_stamp;
// ... some more data is assigned to the gps_data object ...
// e.g. already transformed, cartesian coordinates (see calculate_equirectual_projection) 
this->gps_fuser.updateGPSData(gps_data);
```

#### Read Fused Data from the GPSFuser library

To read the currently fused odometry, the `get_current_pose(double current_time_in_secs, SensorDataClasses::OdometryData& pose)` function has to be used.
A simple usage example is given below:

```c++
SensorDataClasses::OdometryData fused_odometry;
if (this->gps_fuser.getCurrentPose(current_time_in_secs, fused_odometry)) {
    // a fused odometry from prior given GPS data is saved in fused_odometry
}
```

#### Calculating an Equirectual Projection

To calculate an euquirectual projection of the GPS data in GPS coordinates to cartesian coordinates as needed by the GPSFuser lib, the `calculate_equirectual_projection(double latitude, double longitude, double altitude, double radius_of_earth, double &x, double &y, double &z)` is provided. This function is recommanded, when a projection should be calculated. An usage example can be found below:

```c++
double x, y, z;
double radius_of_earth = 6365084;  // example radius at a certain position at Fraunhofer IML
this->gps_fuser.calculateEquirectualProjection(latitude, longitude, altitude, radius_of_earth, x, y, z);
// x, y, z contains now the calculated equirectual projection of the GPS coordinates given as 
// latitude, longitude, altitude
```

#### Calculating an WSG84 Projection

To calculate an euquirectual projection of the GPS data in GPS coordinates to cartesian coordinates as needed by the GPSFuser lib, the `calculate_equirectual_projection(double latitude, double longitude, double altitude, double &x, double &y, double &z)` is provided. An usage example can be found below:

```c++
double x, y, z;
this->gps_fuser.calculateWGS84Projection(latitude, longitude, altitude, x, y, z);
// x, y, z contains now the calculated WSG84 projection of the GPS coordinates given as 
// latitude, longitude, altitude
```

## Authors

Further questions can be discussed by opening a new issue for this repository or by writing an E-Mail to [sebastian.hoose@iml.fraunhofer.de](mailto:sebastian.hoose@iml.fraunhofer.de).

## License

This project is licensed under the Open Logistics License 1.3. For more information see the LICENSE file in this repository or visit the website of the
[Open Logistics Foundation](https://www.openlogisticsfoundation.org/).

