image: registry.gitlab.cc-asp.fraunhofer.de/silicon-economy/services/odyn/sensory/docker/sensory-image:focal-latest

stages:
  - build
  - test
  - code-analysis
  - documentation

build_tests:
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_DEPTH: 0
  script:
    - mkdir -p build && cd build
    - cmake .. -DBUILD_TESTS=ON -DCODE_COVERAGE=ON -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang
    - cmake --build . --target ccov-gps_fuser_tests
  artifacts:
    paths:
      - build/
      - docs/
    expire_in: 1 week
  rules:
    - if: ($CI_COMMIT_BRANCH == "main") || ($CI_COMMIT_BRANCH == "dev")
      allow_failure: false
    - when: on_success
      allow_failure: true

build_debug:
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_DEPTH: 0
  script:
    - mkdir -p build && cd build
    - cmake -DCMAKE_BUILD_TYPE=Debug ..
    - make -j14
  artifacts:
    paths:
      - build/
      - docs/
    expire_in: 1 week
  rules:
    - if: ($CI_COMMIT_BRANCH == "main") || ($CI_COMMIT_BRANCH == "dev")
      allow_failure: false
    - when: on_success
      allow_failure: true

build_release:
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    GIT_DEPTH: 0
  script:
    - mkdir -p build && cd build
    - cmake -DCMAKE_BUILD_TYPE=Release ..
    - make -j14
  artifacts:
    paths:
      - build/
      - docs/
    expire_in: 1 week
  rules:
    - if: ($CI_COMMIT_BRANCH == "main") || ($CI_COMMIT_BRANCH == "dev")
      allow_failure: false
    - when: on_success
      allow_failure: true

catch2_test:
  stage: test
  dependencies:
    - "build_tests"
  needs: ['build_tests']
  script:
    - cd build
    - ./gps_fuser_tests
  rules:
    - if: ($CI_COMMIT_BRANCH == "main") || ($CI_COMMIT_BRANCH == "dev")
      allow_failure: false
    - when: on_success
      allow_failure: true

sonarqube:
  stage: test
  dependencies:
    - "build_tests"
  needs: ['build_tests']
  before_script:
    - wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.2.2472-linux.zip > /dev/null
    - unzip sonar-scanner-cli-4.6.2.2472-linux.zip > /dev/null
    - rm sonar-scanner-cli-4.6.2.2472-linux.zip
    - git fetch --unshallow || true
    - cd /builds/silicon-economy/services/odyn/sensory/gps-fuser/gps_fuser
    - wget $SONAR_HOST_URL/static/cpp/build-wrapper-linux-x86.zip > /dev/null
    - unzip -j build-wrapper-linux-x86.zip
    - mkdir build_wrapper_output_directory
    - ./build-wrapper-linux-x86-64 --out-dir build_wrapper_output_directory /usr/bin/cmake --build /builds/silicon-economy/services/odyn/sensory/gps-fuser/gps_fuser/build --config Debug --clean-first --target all -- -j 14
  script:
    - llvm-cov show ./build/gps_fuser_tests -instr-profile=build/gps_fuser_tests.profdata > coverage.txt
    - ./sonar-scanner-4.6.2.2472-linux/bin/sonar-scanner
      -Dsonar.projectKey=odyn.sensory.gps-fuser
      -Dsonar.projectName=odyn.sensory.gps-fuser
      -Dsonar.cfamily.build-wrapper-output=build_wrapper_output_directory
      -Dsonar.host.url=$SONAR_HOST_URL
      -Dsonar.login=$SONAR_TOKEN
      -Dsonar.sourceEncoding=UTF-8
      -Dsonar.cfamily.threads=1
      -Dsonar.cfamily.cache.enabled=false
      -Dsonar.cfamily.llvm-cov.reportPath=coverage.txt
      -Dsonar.qualitygate.wait=true
      -Dsonar.exclusions=test/*
  cache:
    key: "sonar-${CI_PROJECT_ID}"
    paths:
      - ".scannerwork"
      - ".sonar"
  rules:
    - if: ($CI_COMMIT_BRANCH == "main") || ($CI_COMMIT_BRANCH == "dev")
      allow_failure: false
    - when: on_success
      allow_failure: true

clang_tidy:
  stage: code-analysis
  script:
    - cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .
    - clang-tidy ./src/* ./include/* -format-style='file'
  needs: []
  rules:
    - if: ($CI_COMMIT_BRANCH == "main") || ($CI_COMMIT_BRANCH == "dev")
      allow_failure: false
    - when: on_success
      allow_failure: true

cppcheck:
  stage: code-analysis
  script:
    - cppcheck --enable=performance --inline-suppr --error-exitcode=1 ./src/ ./include/
  needs: []
  rules:
    - if: ($CI_COMMIT_BRANCH == "main") || ($CI_COMMIT_BRANCH == "dev")
      allow_failure: false
    - when: on_success
      allow_failure: true
