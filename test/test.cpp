// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3


// Test of the GPSFuser library using Catch2
// Author: Sebastian Hoose (sebastian.hoose@iml.fraunhofer.de)

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

#include <Eigen/Dense>  // NOLINT(build/include_order)
#include <random>  // NOLINT(build/include_order)

#define private public
#include "gps_fuser.h"
#include "data_classes.h"

// Tests of sensor_data_classes

TEST_CASE("Test correct initialization of sensor_data_classes::Pose") {
    sensor_data_classes::Pose pose;
    REQUIRE(pose.position.x() == Approx(0.0));
    REQUIRE(pose.position.y() == Approx(0.0));
    REQUIRE(pose.position.z() == Approx(0.0));

    REQUIRE(pose.orientation.x() == Approx(0.0));
    REQUIRE(pose.orientation.y() == Approx(0.0));
    REQUIRE(pose.orientation.z() == Approx(0.0));
    REQUIRE(pose.orientation.w() == Approx(1.0));
}

TEST_CASE("Test correct initialization of sensor_data_classes::PoseWithCovariance") {
    sensor_data_classes::PoseWithCovariance pose_with_covariance;
    REQUIRE(pose_with_covariance.position.x() == Approx(0.0));
    REQUIRE(pose_with_covariance.position.y() == Approx(0.0));
    REQUIRE(pose_with_covariance.position.z() == Approx(0.0));
    REQUIRE(pose_with_covariance.orientation.x() == Approx(0.0));
    REQUIRE(pose_with_covariance.orientation.y() == Approx(0.0));
    REQUIRE(pose_with_covariance.orientation.z() == Approx(0.0));
    REQUIRE(pose_with_covariance.orientation.w() == Approx(1.0));
    REQUIRE(pose_with_covariance.position_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(pose_with_covariance.orientation_covariance == Eigen::Matrix3d::Identity());
}

TEST_CASE("Test correct initialization of sensor_data_classes::TwistWithCovariance") {
    sensor_data_classes::TwistWithCovariance twist_with_covariance;
    REQUIRE(twist_with_covariance.linear.x() == Approx(0.0));
    REQUIRE(twist_with_covariance.linear.y() == Approx(0.0));
    REQUIRE(twist_with_covariance.linear.z() == Approx(0.0));
    REQUIRE(twist_with_covariance.angular.x() == Approx(0.0));
    REQUIRE(twist_with_covariance.angular.y() == Approx(0.0));
    REQUIRE(twist_with_covariance.angular.z() == Approx(0.0));
    REQUIRE(twist_with_covariance.linear_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(twist_with_covariance.angular_covariance == Eigen::Matrix3d::Identity());
}

TEST_CASE("Test correct initialization of sensor_data_classes::OdometryData") {
    sensor_data_classes::OdometryData odometry_data;
    REQUIRE(odometry_data.time_stamp == 0.0);
    REQUIRE(odometry_data.pose.position.x() == Approx(0.0));
    REQUIRE(odometry_data.pose.position.y() == Approx(0.0));
    REQUIRE(odometry_data.pose.position.z() == Approx(0.0));
    REQUIRE(odometry_data.pose.orientation.x() == Approx(0.0));
    REQUIRE(odometry_data.pose.orientation.y() == Approx(0.0));
    REQUIRE(odometry_data.pose.orientation.z() == Approx(0.0));
    REQUIRE(odometry_data.pose.orientation.w() == Approx(1.0));
    REQUIRE(odometry_data.pose.position_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(odometry_data.pose.orientation_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(odometry_data.twist.linear.x() == Approx(0.0));
    REQUIRE(odometry_data.twist.linear.y() == Approx(0.0));
    REQUIRE(odometry_data.twist.linear.z() == Approx(0.0));
    REQUIRE(odometry_data.twist.angular.x() == Approx(0.0));
    REQUIRE(odometry_data.twist.angular.y() == Approx(0.0));
    REQUIRE(odometry_data.twist.angular.z() == Approx(0.0));
    REQUIRE(odometry_data.twist.linear_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(odometry_data.twist.angular_covariance == Eigen::Matrix3d::Identity());
}

TEST_CASE("Test correct initialization of sensor_data_classes::GPSData") {
    sensor_data_classes::GPSData gps_data;
    REQUIRE(gps_data.time_stamp == Approx(0.0));
    REQUIRE(gps_data.origin_frame_id == "");
    REQUIRE(gps_data.data.x() == Approx(0.0));
    REQUIRE(gps_data.data.y() == Approx(0.0));
    REQUIRE(gps_data.data.z() == Approx(0.0));
    REQUIRE(gps_data.translation_to_base.x() == Approx(0.0));
    REQUIRE(gps_data.translation_to_base.y() == Approx(0.0));
    REQUIRE(gps_data.translation_to_base.z() == Approx(0.0));
    REQUIRE(gps_data.position_covariance.empty());
    REQUIRE(gps_data.position_covariance_type == sensor_data_classes::GPSData::CovarianceType::COVARIANCE_TYPE_UNKNOWN);
}

// Tests of GPSFuser

/**
 * \brief Generation of test GPS data
 */
sensor_data_classes::GPSData generateTestSensorData() {
    sensor_data_classes::GPSData gps_data;
    static double increasing_time_stamp = 0.0;
    increasing_time_stamp += 0.001;

    gps_data.time_stamp = increasing_time_stamp;

    static std::uniform_real_distribution<double> unif(-0.5 , 0.5);
    static std::default_random_engine random_engine;
    gps_data.data.x() = unif(random_engine);
    gps_data.data.y() = unif(random_engine);
    gps_data.data.z() = unif(random_engine);
    return gps_data;
}

TEST_CASE("Test correct initialization using the standard GPSFuser constructor") {
    GPSFuser gps_fuser;

    REQUIRE(gps_fuser.last_fused_odometry_data.time_stamp == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.position.x() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.position.y() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.position.z() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation.x() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation.y() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation.z() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation.w() == Approx(1.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.position_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.linear.x() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.linear.y() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.linear.z() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.angular.x() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.angular.y() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.angular.z() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.linear_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.angular_covariance == Eigen::Matrix3d::Identity());

    REQUIRE(gps_fuser.all_current_gps_data.empty());

    REQUIRE(gps_fuser.initial_position_in_world.position.x() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.position.y() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.position.z() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.orientation.x() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.orientation.y() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.orientation.z() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.orientation.w() == Approx(1.0));

    REQUIRE(gps_fuser.initial_position_in_world_has_been_calculated == false);
}


TEST_CASE("Test correct initialization using the non default GPSFuser constructor") {
    sensor_data_classes::Pose initial_pose;
    const double max_time_delay = 5.0;
    GPSFuser gps_fuser(max_time_delay, initial_pose);

    REQUIRE(gps_fuser.max_time_delay == max_time_delay);

    REQUIRE(gps_fuser.last_fused_odometry_data.time_stamp == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.position.x() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.position.y() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.position.z() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation.x() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation.y() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation.z() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation.w() == Approx(1.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.position_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(gps_fuser.last_fused_odometry_data.pose.orientation_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.linear.x() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.linear.y() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.linear.z() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.angular.x() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.angular.y() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.angular.z() == Approx(0.0));
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.linear_covariance == Eigen::Matrix3d::Identity());
    REQUIRE(gps_fuser.last_fused_odometry_data.twist.angular_covariance == Eigen::Matrix3d::Identity());

    REQUIRE(gps_fuser.all_current_gps_data.empty());

    REQUIRE(gps_fuser.initial_position_in_world.position.x() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.position.y() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.position.z() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.orientation.x() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.orientation.y() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.orientation.z() == Approx(0.0));
    REQUIRE(gps_fuser.initial_position_in_world.orientation.w() == Approx(1.0));

    REQUIRE(gps_fuser.initial_position_in_world_has_been_calculated == false);
}

TEST_CASE("Testing calculate_initial_pose_in_world") {
    GPSFuser gps_fuser;
    std::vector<sensor_data_classes::GPSData> sensor_data;

    sensor_data_classes::GPSData data_1 = generateTestSensorData();
    data_1.data = Eigen::Vector3d(0.0, 0.0, 0.0);
    data_1.origin_frame_id = "data_1_frame";
    sensor_data_classes::GPSData data_2 = generateTestSensorData();
    data_2.data = Eigen::Vector3d(0.0, 0.0, 0.0);
    data_2.origin_frame_id = "data_2_frame";

    sensor_data.push_back(data_1);
    sensor_data.push_back(data_2);

    sensor_data_classes::Pose pose;
    pose = gps_fuser.calculateInitialPoseInWorld(sensor_data);
    // not using == Eigen::Vector::Zero() because of rounding errors
    REQUIRE(std::abs(pose.position.x()) < 0.000000001);
    REQUIRE(std::abs(pose.position.y()) < 0.000000001);
    REQUIRE(std::abs(pose.position.z()) < 0.000000001);
}

TEST_CASE("Testing calculate_angle_between_points") {
    GPSFuser gps_fuser;
    Eigen::Vector3d v1(1.0, 0.0, 0.0);
    Eigen::Vector3d v2(2.0, 0.0, 0.0);

    double result = gps_fuser.calculateAngleBetweenPoints(v1, v2);
    REQUIRE(result == 0.0);
}

TEST_CASE("Testing check_for_old_data") {
    GPSFuser gps_fuser;
    gps_fuser.max_time_delay = 0.1;

    sensor_data_classes::GPSData gps_data_1 = generateTestSensorData();
    sensor_data_classes::GPSData gps_data_2 = generateTestSensorData();

    gps_fuser.all_current_gps_data.push_back(gps_data_1);
    gps_fuser.all_current_gps_data.push_back(gps_data_2);

    REQUIRE(gps_fuser.checkForOldData(gps_data_2.time_stamp));
    REQUIRE(gps_fuser.checkForOldData(gps_data_2.time_stamp + 0.001));
    REQUIRE(!gps_fuser.checkForOldData(gps_data_2.time_stamp + 0.11));
}

TEST_CASE("Testing update_size_of_rotation_tracking_vectors") {
    GPSFuser gps_fuser;
    std::vector<sensor_data_classes::GPSData> sensor_data;

    sensor_data_classes::GPSData data_1 = generateTestSensorData();
    data_1.origin_frame_id = "data_1_frame";
    sensor_data_classes::GPSData data_2 = generateTestSensorData();
    data_2.origin_frame_id = "data_2_frame";

    gps_fuser.all_current_gps_data.push_back(data_1);
    gps_fuser.all_current_gps_data.push_back(data_2);

    std::vector<std::vector<Eigen::Vector3d>> rotation_tracking_vectors;
    gps_fuser.updateSizeOfRotationTrackingVectors(rotation_tracking_vectors);
    REQUIRE(rotation_tracking_vectors.size() == 2);
    REQUIRE(rotation_tracking_vectors[0].size() == 1);
    REQUIRE(rotation_tracking_vectors[1].size() == 1);

    sensor_data_classes::GPSData data_3 = generateTestSensorData();
    data_2.origin_frame_id = "data_3_frame";
    gps_fuser.all_current_gps_data.push_back(data_3);
    gps_fuser.updateSizeOfRotationTrackingVectors(rotation_tracking_vectors);
    REQUIRE(rotation_tracking_vectors.size() == 3);
    REQUIRE(rotation_tracking_vectors[0].size() == 2);
    REQUIRE(rotation_tracking_vectors[1].size() == 2);
    REQUIRE(rotation_tracking_vectors[2].size() == 2);

    sensor_data_classes::GPSData data_4 = generateTestSensorData();
    data_2.origin_frame_id = "data_4_frame";
    gps_fuser.all_current_gps_data.push_back(data_4);
    gps_fuser.updateSizeOfRotationTrackingVectors(rotation_tracking_vectors);
    REQUIRE(rotation_tracking_vectors.size() == 4);
    REQUIRE(rotation_tracking_vectors[0].size() == 3);
    REQUIRE(rotation_tracking_vectors[1].size() == 3);
    REQUIRE(rotation_tracking_vectors[2].size() == 3);
    REQUIRE(rotation_tracking_vectors[3].size() == 3);
}

// public functions

TEST_CASE("Testing update_gps_data") {
    GPSFuser gps_fuser;
    sensor_data_classes::GPSData data_1 = generateTestSensorData();
    data_1.origin_frame_id = "data_1_frame";
    sensor_data_classes::GPSData data_2 = generateTestSensorData();
    data_2.origin_frame_id = "data_2_frame";

    gps_fuser.updateGPSData(data_1);
    REQUIRE(gps_fuser.all_current_gps_data.size() == 1);
    gps_fuser.updateGPSData(data_1);
    REQUIRE(gps_fuser.all_current_gps_data.size() == 1);
    gps_fuser.updateGPSData(data_2);
    REQUIRE(gps_fuser.all_current_gps_data.size() == 2);
}

TEST_CASE("Testing get_current_pose for a single sensor") {
    GPSFuser gps_fuser;
    sensor_data_classes::GPSData data_1 = generateTestSensorData();
    data_1.data = Eigen::Vector3d(0.0, 0.0, 0.0);
    data_1.origin_frame_id = "data_1_frame";
    sensor_data_classes::GPSData data_2 = generateTestSensorData();
    data_2.data = Eigen::Vector3d(1.0, 0.0, 0.0);
    data_2.origin_frame_id = "data_1_frame";

    sensor_data_classes::OdometryData odom;
    gps_fuser.updateGPSData(data_1);
    gps_fuser.getCurrentPose(data_1.time_stamp, odom);
    gps_fuser.updateGPSData(data_2);
    gps_fuser.getCurrentPose(data_2.time_stamp, odom);

    REQUIRE(odom.pose.position.x() == Approx(1.0));
    REQUIRE(odom.pose.position.y() == Approx(0.0));
    REQUIRE(odom.pose.orientation.x() == Approx(0.0));
    REQUIRE(odom.pose.orientation.y() == Approx(0.0));
    REQUIRE(odom.pose.orientation.z() == Approx(0.0));
    REQUIRE(odom.pose.orientation.w() == Approx(1.0));

    REQUIRE(odom.twist.linear.x() > 0.0);
    REQUIRE(odom.twist.linear.y() == Approx(0.0));
    REQUIRE(odom.twist.angular.x() == Approx(0.0));
    REQUIRE(odom.twist.angular.y() == Approx(0.0));
    REQUIRE(odom.twist.angular.z() == Approx(0.0));
}

TEST_CASE("Testing get_current_pose for two sensors") {
    GPSFuser gps_fuser;
    sensor_data_classes::GPSData data_1_1 = generateTestSensorData();
    data_1_1.data = Eigen::Vector3d(0.0, 0.0, 0.0);
    data_1_1.origin_frame_id = "data_1_frame";
    sensor_data_classes::GPSData data_1_2 = generateTestSensorData();
    data_1_2.data = Eigen::Vector3d(1.0, 0.0, 0.0);
    data_1_2.origin_frame_id = "data_1_frame";

    sensor_data_classes::GPSData data_2_1 = generateTestSensorData();
    data_2_1.data = Eigen::Vector3d(0.0, 0.0, 0.0);
    data_2_1.origin_frame_id = "data_2_frame";
    sensor_data_classes::GPSData data_2_2 = generateTestSensorData();
    data_2_2.data = Eigen::Vector3d(1.0, 0.0, 0.0);
    data_2_2.origin_frame_id = "data_2_frame";

    sensor_data_classes::OdometryData odom;
    gps_fuser.updateGPSData(data_1_1);
    gps_fuser.updateGPSData(data_2_1);
    gps_fuser.getCurrentPose(data_1_1.time_stamp, odom);
    gps_fuser.updateGPSData(data_1_2);
    gps_fuser.updateGPSData(data_2_2);
    gps_fuser.getCurrentPose(data_1_2.time_stamp, odom);

    REQUIRE(std::abs(odom.pose.position.x() - 1.0) < 0.1);
    REQUIRE(std::abs(odom.pose.position.y()) < 0.1);
    REQUIRE(odom.pose.orientation.x() == Approx(0.0));
    REQUIRE(odom.pose.orientation.y() == Approx(0.0));
    REQUIRE(odom.pose.orientation.z() == Approx(0.0));
    REQUIRE(odom.pose.orientation.w() == Approx(1.0));

    REQUIRE(odom.twist.linear.x() > 0.0);
    REQUIRE(odom.twist.linear.y() >= 0.0);
    REQUIRE(odom.twist.angular.x() == Approx(0.0));
    REQUIRE(odom.twist.angular.y() == Approx(0.0));
    REQUIRE(odom.twist.angular.z() == Approx(0.0));
}

TEST_CASE("Testing calculate_equirectual_projection") {
    double latitude = 51.494435;
    double longitude = 7.407444;
    double altitude = 165.4;
    double x, y, z;
    double x_1, y_1, z_1;

    GPSFuser::calculateEquirectualProjection(latitude, longitude, altitude, 6365084.0, x, y, z);
    GPSFuser::calculateEquirectualProjection(latitude + 0.0001, longitude + 0.0001, altitude, 6365084.0, x_1, y_1, z_1);
    REQUIRE(x > 0.0);
    REQUIRE(y > 0.0);
    REQUIRE(z == 165.4);
    REQUIRE(x_1 - x < 0.0);
    REQUIRE(y_1 - y > 0.0);
    REQUIRE(z_1 - z == 0.0);
}
